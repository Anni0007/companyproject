create database click2cloud;
use click2cloud;

create table user (id integer primary key auto_increment, Name varchar(30),
email varchar(50) unique, password varchar(200));

create table project_details ( id integer primary key auto_increment, title varchar(200),
description varchar(250), user_id integer not null , FOREIGN KEY (user_id) REFERENCES user(id) ON DELETE CASCADE );

create table task_details(id integer primary key auto_increment, task_title varchar(200), task_description text,
priority varchar(50), deadline date, status varchar(20), project_id integer not null , 
FOREIGN KEY (project_id) REFERENCES project_details(id) ON DELETE CASCADE);

create table comment_tbl( id integer primary key auto_increment, comment_description text , task_id integer not null , 
FOREIGN KEY (task_id) REFERENCES task_details(id) ON DELETE CASCADE);