const express = require('express')
const db = require('../db')
const utils = require('../utils')
const router = express.Router()

// api for posting comment on associated task
router.post('/comment/:task_id', (request,response)=>{
    const {task_id} = request.params
    const {comment_description} = request.body
    const statement  = `insert into comment_tbl (comment_description,task_id) values('${comment_description}', '${task_id}');`
    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
} )

// api for delete single comment from any task
router.delete('/comment/:id', (request,response)=>{
    const {id} = request.params
    const statement  = `delete from comment_tbl where id =  '${id}';`
    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
} )

// api for deleting all comment from any task
router.delete('/task/comment/:task_id', (request,response)=>{
    const {task_id} = request.params
    const statement  = `delete from comment_tbl where task_id =  '${task_id}';`
    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })
} )

module.exports = router