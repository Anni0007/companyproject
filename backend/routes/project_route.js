const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()

// z API for get specific project by project id 
router.get('/projects/:project_id', (request, response) => {
    const {project_id} = request.params
    const statement = `select title, description from project_details where id  = '${project_id}'  && user_id = '${request.id}';`
    db.execute(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
})

//API for get all the projects for specific user
router.get('/projects', (request, response) => {
    const statement = `select id, title, description from project_details where user_id = '${request.id}';`
    db.execute(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
})

//APi for creating the  projects 
router.post('/projects', (request, response) => {
    const {title, description} = request.body
    console.log(title,description)

    const statement = `insert into project_details (title, description, user_id) values ('${title}','${description}', '${request.id}');`
    db.execute(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
})

//Api for updating the project ddetails 
router.put('/projects/:project_id', (request, response) => {
    const {project_id} = request.params
    const { title, description } = request.body
    const statement = `update project_details set 
          title = '${title}',
          description = '${description}'
          where id = '${project_id}' && user_id = '${request.id}'`
    db.execute(statement, (error, data) => {
      response.send(utils.createResult(error, data))
    })
})

// API for delete projects with project_id - single project deleted by this
router.delete('/projects/:project_id', (request, response) => {
  const { project_id } = request.params

  const statement = `delete from project_details where id = '${project_id}' && user_id = '${request.id}'`
  db.execute(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

// API for delete all projects (button from front end side)
router.delete('/projects', (request, response) => {
  const { project_id } = request.params

  const statement = `delete from project_details where user_id = '${request.id}'`
  db.execute(statement, (error, data) => {
    response.send(utils.createResult(error, data))
  })
})

module.exports = router
