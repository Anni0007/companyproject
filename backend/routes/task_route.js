const express = require('express')
const db = require('../db')
const utils = require('../utils')

const router = express.Router()
const mysql2 = require('mysql2/promise')
const pool = mysql2.createPool({
    host: 'localhost',
    user: 'root',
    password: 'root',
    port: 3306,
    database: 'click2cloud',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0,
  })

//API to extract all task of specific projects by project_id
router.get('/project/tasks/:project_id', (request,response)=>{
    const {project_id} = request.params

    const statement = `select id, task_title, task_description, priority, deadline,
                      status from task_details where project_id = '${project_id}';`

    db.execute(statement, (error, data) => {
    response.send(utils.createResult(error, data))
    })
})

// API to create task by project_id
router.post('/project/tasks/:project_id', (request,response)=>{
    const {project_id} = request.params
    const {task_title, task_description, priority, deadline, status } = request.body

    const statement = `insert into task_details (task_title, task_description, priority, deadline,
                      status, project_id) values('${task_title}', '${task_description}', '${priority}',
                      '${deadline}', '${status}', '${project_id}');`

    db.execute(statement, (error, data) => {
    response.send(utils.createResult(error, data))
    })
})


// Api to get single task with associated comments of that task
router.get('/project/:project_id/task/:task_id', (request,response)=>{
    const {project_id, task_id} = request.params
    ;(async () => {
    const statement = ` select task_title, task_description, priority, deadline,
    status from task_details where project_id = '${project_id}' && id = '${task_id}'`

    const statement2 = ` select id as comment_id, comment_description
        from comment_tbl where task_id = '${task_id}';`

        const final_data = []
        const[task_data] = await pool.execute(statement)
        final_data.push(task_data)

         const[comment_data] = await pool.execute(statement2)
         final_data.push(comment_data)

        console.log(task_data,comment_data)
        response.send(final_data)
    })()

})

//Api to update the task by task_id 
router.put('/project/task/:task_id',(request,response)=>{
    const { task_id } = request.params
    const { task_title, task_description, priority , deadline , status } = request.body
    const statement =  `update task_details set 
    task_title = '${task_title}',
    task_description = '${task_description}',
    priority = '${priority}',
    deadline = '${deadline}',
    status = '${status}'
    where id = '${task_id}';`

    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

// API to update the priority
router.patch('/project/task/update_priority/:task_id',(request,response)=>{
    const { task_id } = request.params
    const { priority } = request.body
    const statement =  `update task_details set 
    priority = '${priority}'
    where id = '${task_id}';`

    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

//API to update the status of task 
router.patch('/project/task/update_status/:task_id',(request,response)=>{
    const { task_id } = request.params
    const { status } = request.body
    const statement =  `update task_details set 
    status = '${status}'
    where id = '${task_id}';`

    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

//API to update the deadline of task 
router.patch('/project/task/update_deadline/:task_id',(request,response)=>{
    const { task_id } = request.params
    const { deadline } = request.body
    const statement =  `update task_details set 
    deadline = '${deadline}'
    where id = '${task_id}';`

    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

// Api to delete the task 
router.delete('/project/task/:task_id',(request,response)=>{
    const { task_id } = request.params
    const statement =  `delete from task_details where id = '${task_id}';`

    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

// Api to delete all the task associated with project
router.delete('/project/:project_id/task/',(request,response)=>{
    const { project_id } = request.params
    const statement =  `delete from task_details where project_id = '${project_id}';`

    db.execute(statement, (error, data) => {
        response.send(utils.createResult(error, data))
    })

})

module.exports = router