const express = require('express')
const db = require('../db')
const crypto = require('crypto-js')
const utils = require('../utils')
const jwt = require('jsonwebtoken')
const config = require('../config')

const router = express.Router()


// User signup Api 

router.post('/user/signup', (request,response)=>{
    // destructuring  request body 
    const { name , email, password} = request.body

  // encrypt the password
  const encryptedPassword = '' + crypto.SHA256(password)

  // by default every user will be non-verified
  const statement = `insert into user (Name,  email, password) values 
                    ('${name}', '${email}', '${encryptedPassword}')`

  db.execute(statement, (error,data)=>{
      const result = utils.createResult(error,data)

      if (!error){
          response.send(result)
      } else 
      {
          response.send(result)
      }
  })
})

// signin api 
router.post('/user/signin' , (request,response)=>{
    const { email , password } = request.body
     // encrypt the password
    const encryptedPassword = '' + crypto.SHA256(password)

    const statement = `select id, Name, email from user 
      where email = '${email}' and password = '${encryptedPassword}'`

    db.execute(statement, (error, users) => {
        const result = {
        status: '',
        }
 
    if (error != null) {
      // error while executing statement
      result['status'] = 'error'
      result['error'] = error

    } else {
        if (users.length == 0) {
            // user does not exist
            result['status'] = 'error'
            result['error'] = 'User does not exist'

        } else {
          const user = users[0]
          const payload = { id: user['id'] }
          const token = jwt.sign(payload, config.secret)

          result['status'] = 'success'
          result['data'] = {
              token: token,
              Name: user['Name'],
              email: user['email'],        
          }
      }   
    }
    response.send(result)
  })
})

 module.exports = router

     