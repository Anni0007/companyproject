const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const jwt = require('jsonwebtoken')
const config = require('./config')
const cors = require('cors')

app.use(cors('*'))
app.use(bodyParser.urlencoded())
app.use(bodyParser.json())

const userRouter = require('./routes/user')
const projectRouter = require('./routes/project_route')
const taskRouter = require('./routes/task_route')
const commentRouter = require('./routes/comment_route')

app.use((request, response, next) => {
    // skip checking the token for following APIs
    // signin and signup
  
    if (
      request.url == '/user/signin' ||
      request.url == '/user/signup'
    ) {
      // skip checking the token
      next()
    } else {
      // get the token from headers
      const token = request.headers['token']
  
      try {
        // verify if the token is original or intact
        const payload = jwt.verify(token, config.secret)
  
        // get id from the token
        // add the user id in the request object so that it can be used
        // in ever other APIs
        request.id = payload['id']
  
        // call the next handler
        next()
      } catch (ex) {
        response.send({
          status: 'error',
          error: 'unauthorized access',
        })
      }
    }
  })


//add routes
app.use(userRouter)
app.use(projectRouter)
app.use(taskRouter)
app.use(commentRouter)

// homepage
app.get('/', (request,response)=>{
    console.log('homepage')
    response.send("homepage")
})



app.listen('4000','0.0.0.0', ()=> {
 console.log("welcome to my app")
})

