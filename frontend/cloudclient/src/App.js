import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import SignupScreen from './screens/SignupScreen';
import SigninScreen from './screens/SigninScreen';
import Navigation from './components/Navigation'
import HomeScreen from './screens/HomeScreen';
import AddProjectScreen from './screens/AddProjectScreen';
import TaskScreen from './screens/TaskScreen';
import TaskDetailScreen from './screens/TaskDetailScreen';
import AddTaskScreen from './screens/AddTaskScreen';


function App() {
  return (
    <div className="App">
      <Router>
        <Navigation/>
        <div className="container">
          <Switch>
            <Route path="/signup" component={SignupScreen} />
            <Route path="/signin" component={SigninScreen} />
            <Route path="/home" component={HomeScreen} />
            <Route path="/addproject" component={AddProjectScreen} />
            <Route path="/task" component={TaskScreen} />
            <Route path="/taskdetail" component={TaskDetailScreen} />
            <Route path="/addtask" component={AddTaskScreen} />
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
