import axios from "axios";

const COMMON_URL = 'http://127.0.0.1:4000';
const SIGNUP = "/user/signup";
const SIGNIN = "/user/signin";
const ADDPROJECTS = "/projects";
const GETALLPROJECTS = "/projects";
const ADDNEWTASK = "/project/tasks/";
const GETALLTASKS = "/project/tasks/"

export const getAllProject = async (token) => {
  return axios
    .get(COMMON_URL + GETALLPROJECTS  ,{headers: {"token" : `${token}`} })
    .then((resp) => resp.data)
    .catch((err) => {
      throw new Error(err);
    });
};

export const getAllTask = async (Id ,token) => {
    return axios
      .get(COMMON_URL + GETALLTASKS  + Id ,{headers: {"token" : `${token}`} })
      .then((resp) => resp.data)
      .catch((err) => {
        throw new Error(err);
      });
  };

export const signUpUser = async (data) => {
    return axios
      .post(COMMON_URL + SIGNUP, data)
      .then((resp) => resp.data)
      .catch((err) => {
        throw new Error(err);
      });
  };
 
export const signInUser = async (data) => {
    return axios
      .post(COMMON_URL + SIGNIN, data)
      .then((resp) => resp.data)
      .catch((err) => {
        throw new Error(err);
      });
  };

  export const addProject = async(data, token) => {
    return axios
      .post(COMMON_URL + ADDPROJECTS,  data, {headers: {"token" : `${token}`} })
      .then((resp) => resp.data)
      .catch((err) => {
        throw new Error(err);
      });
  };

  export const addNewTask = async(id, data, token) => {
    return axios
      .post(COMMON_URL + ADDNEWTASK + id, data, {headers: {"token" : `${token}`} })
      .then((resp) => resp.data)
      .catch((err) => {
        throw new Error(err);
      });
  };
