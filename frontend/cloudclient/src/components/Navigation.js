import { Link } from 'react-router-dom'

const Navigation = (props) => {

    const logout = ()=>{
        sessionStorage.removeItem("token")
    }
    
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <Link to="/home">
                    <span className="navbar-brand">Project Managment App </span>
                    </Link>
                    <div
                    className="collapse navbar-collapse"
                    id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                        <Link to="/home">
                            <span className="nav-link">Home</span>
                        </Link>
                        </li>

                        <li className="nav-item">
                        <Link to="/about">
                            <span className="nav-link">About</span>
                        </Link>
                        </li>

                    </ul>

                   { sessionStorage.getItem('token') && <div className="d-flex">
                        <button className="btn btn-outline-success" onClick={logout}>
                        Logout
                        </button>
                    </div>}
                    </div>
                </div>
            </nav>
    </div>
    )
}

export default Navigation