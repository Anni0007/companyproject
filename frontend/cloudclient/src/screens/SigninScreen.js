import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Link } from 'react-router-dom'
import { signInUser } from '../api'
import Header from '../components/Header'

const SigninScreen = (props) => {

    let history = useHistory()
	const [Email, setEmail] = useState('')
	const [Password, setPassword] = useState('')
	const signIn =async ()=> {
		const payload = {
			email:Email,
			password:Password
		}
		console.log(payload)
		const data = await signInUser(payload)
		if (data.status === 'success'){
		
			sessionStorage.setItem("token" , data['token'])
			sessionStorage.setItem("token", data.data.token);
			const token = sessionStorage.getItem("token");
            console.log(token)
            history.push('/home')
		}
	}
    return (
        <div>
      <Header title="Signin" />
      <div className="form">
        <div className="mb-3">
          <label className="form-label">Email</label>
          <input
            type="email"
            className="form-control"
            placeholder="test@test.com"
            onChange={(e)=>setEmail(e.target.value)}
          />
        </div>
        <div className="mb-3">
          <label className="form-label">Password</label>
          <input
            className="form-control"
            type="password"
            placeholder="*****"
            onChange={(e)=>setPassword(e.target.value)}></input>
        </div>
        <div className="mb-3">
          <button className="btn btn-success" onClick={signIn}>
            Singin
          </button>
          <div className="float-end">
            New User? <Link to="/signup">Signup here</Link>
          </div>
        </div>
      </div>
    </div>
    )
}

export default SigninScreen