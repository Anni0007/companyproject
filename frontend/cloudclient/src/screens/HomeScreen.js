import { Link } from 'react-router-dom'
import Header from '../components/Header'
import { useState, useEffect } from 'react'
import { getAllProject } from '../api'
import { useHistory } from 'react-router-dom'



const HomeScreen = (props)=> {
    let history = useHistory()
    
    const projectId = (id)=> { 
        history.push({
        pathname: '/task',  state: id
    })}
    const [projectData, setProjectData] = useState([]) 
 
     useEffect(async () => {
        await getAllData();
      }, []);
    
      const getAllData = async () => {
        try {
        const token = sessionStorage.getItem("token");
          const data = await getAllProject(token);
          setProjectData(data.data)
        } catch (e) {
          console.error(e);
        }
      };
    
  return (
    <div>
      <Header title="Project" />
            <Link to="/addproject">
                <button
                    className="btn btn-sm btn-success btn-add-task float-right">
                    Create New Project
                </button>
            </Link>

      <div className="row">
        {projectData.map((project) => {
          return (
            <div className="project col-md-3">
              <div className="title">{project.title}</div>
              <div className="description">{project.description}</div>
              
                <button className="btn btn-sm btn-success btn-project-details" onClick={()=>projectId(project.id)} >
                    Project Details
                </button>
            </div>
          )
        })}
      </div>
    </div>
    )
}

export default HomeScreen