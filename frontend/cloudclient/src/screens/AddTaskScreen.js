import { Link } from 'react-router-dom'
import Header from '../components/Header'
import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { addNewTask } from '../api'
import { useLocation } from "react-router-dom";

const AddTaskScreen = (props)=> {
    const location = useLocation();
    console.log(location.state)

    let history = useHistory()
	const [Title, setTitle] = useState('')
	const [Description, setDescription] = useState('')
    const [Priority, setPriority] = useState('')
    const [Deadline, setDeadline] = useState('')
    const [Status, setStatus] = useState('')
    const token = sessionStorage.getItem("token");
	const addNewTaskOnProject =async ()=> {
		const payload = {
			task_title:Title,
			task_description:Description,
            priority:Priority,
            deadline:Deadline,
            status:Status
		}
		// console.log(payload)
		const data = await addNewTask(location.state, payload,token)
		if (data.status === 'success'){
            history.push('/task')
		}
	}
    return(
        <div>
            <Header title="Add Task" />
            <div className="form">
                <div className="mb-3">
                    <label className="form-label">Title</label>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="title"
                        onChange={(e)=>setTitle(e.target.value)}
                    />
                </div>
                <div className="mb-3">
                    <label className="form-label">Description </label>
                    <textarea
                        className="form-control"
                        rows="3"
                        onChange={(e)=>setDescription(e.target.value)}
                    ></textarea>
                </div>
                <div className="mb-3">
                    <label className="form-label">Priority </label>
                    <textarea
                        onChange={(e)=>setPriority(e.target.value)}
                        className="form-control"
                        rows="3"></textarea>
                </div>
                <div className="mb-3">
                    <label className="form-label">Deadline</label>
                    <textarea
                        onChange={(e)=>setDeadline(e.target.value)}
                        className="form-control"
                        rows="3"></textarea>
                </div>
                <div className="mb-3">
                    <label className="form-label">Status</label>
                    <textarea
                        onChange={(e)=>setStatus(e.target.value)}
                        className="form-control"
                        rows="3"></textarea>
                </div>
                <div className="mb-3">
                    <button className="btn btn-success" onClick={addNewTaskOnProject}>
                        Add Task
                    </button>
                    <Link to="/task"> 
                        <button className="btn btn-danger float-end">
                            Cancel
                        </button>
                    </Link>
                </div>
            </div>
        </div> 
    )
}

export default AddTaskScreen