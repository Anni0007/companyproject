import { Link } from 'react-router-dom'
import Header from '../components/Header'
import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { addProject } from '../api'

const AddProjectScreen = (props)=> {
    let history = useHistory()
	const [Title, setTitle] = useState('')
	const [Description, setDescription] = useState('')
    const token = sessionStorage.getItem("token");
	const addProjectDetails =async ()=> {
		const payload = {
			title:Title,
			description:Description

		}
		// console.log(payload)
		const data = await addProject(payload,token)
		if (data.status === 'success'){
            history.push('/home')
		}
	}
    return(
        <div>
            <Header title="Add Project" />
            <div className="form">
                <div className="mb-3">
                    <label className="form-label"> Project Title</label>
                    <input
                        type="text"
                        className="form-control"
                        placeholder="title"
                        onChange={(e)=>setTitle(e.target.value)}
                    />
                </div>
                <div className="mb-3">
                    <label className="form-label">Project Description </label>
                    <textarea
                        className="form-control"
                        rows="3"
                        onChange={(e)=>setDescription(e.target.value)}></textarea>
                </div>
                <div className="mb-3">
                    <button className="btn btn-success" onClick={addProjectDetails}>
                        Add Project
                    </button>
                    <Link to="/home"> 
                        <button className="btn btn-danger float-end">
                            Cancel
                        </button>
                    </Link>
                </div>
            </div>
        </div> 
    )
}

export default AddProjectScreen