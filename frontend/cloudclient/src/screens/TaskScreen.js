import { Link } from 'react-router-dom'
import Header from '../components/Header'
import { useLocation } from "react-router-dom";
import { useEffect, useState } from 'react';
import { getAllTask } from '../api';



const TaskScreen = (props)=>{
    const [taskData, setTaskData] = useState([])
    const [task_id,setTaskId] = useState('')
    const location = useLocation();
    useEffect(async () => {
        setTaskId(location.state)  
        await getAllData(location.state);
      }, []);
      const getAllData = async (Id) => {
        try {
        const token = sessionStorage.getItem("token");
          const data = await getAllTask(Id ,token);
          setTaskData(data.data)
        } catch (e) {
          console.error(e);
        }
      };
    

  return (
    <div>
      <Header title="Task Details" />
            <Link  to={{
                pathname: '/addtask',
                state: task_id
            }}>
                <button
                    className="btn btn-sm btn-success btn-add-task float-right">
                    Create Task
                </button>
            </Link>

      <div className="row">
        {taskData.map((task) => {
          return (
            <div className="taskdetail col-md-3">
              <div className="title">Title :{task.task_title}</div>
              <div className="description">Description :{task.task_description}</div>
              <div>Priority : {task.priority}</div>
              <div>Deadline :{task.deadline}</div>
              <div>Status :{task.status}</div>
          
              <Link to="/taskdetail">
                <button className="btn btn-sm btn-success btn-project-details" >
                    TaskDetails
                </button>
              </Link>
            </div>
          )
        })}
      </div>
    </div>
    )
}

export default TaskScreen