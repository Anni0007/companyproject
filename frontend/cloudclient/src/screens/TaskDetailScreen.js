import { Link } from 'react-router-dom'
import Header from '../components/Header'

const TaskDetailScreen = (props)=> {
    const taskdetail = { id: 1, task_title: 'product 1', task_description: 'description 1', priority: 'medium', deadline: '2022-04-19',status: 'pending'}
    const comments = [{id:1 ,comment_description : "sadfgdj dsnlfs sdjdslf fdsfnsdls"},
    {id:1 ,comment_description : "sadfgdj dsnlfs sdjdslf fdsfnsdls"},
    {id:1 ,comment_description : "sadfgdj dsnlfs sdjdslf fdsfnsdls"},
    {id:1 ,comment_description : "sadfgdj dsnlfs sdjdslf fdsfnsdls"},
    {id:1 ,comment_description : "sadfgdj dsnlfs sdjdslf fdsfnsdls"},
    ]
    return (
        <div>
            <Header title="Task Details" />
            <div>
                <button className="btn btn-success float-start">
                    Update
                </button>
            
                <button className="btn btn-danger float-end">
                    Delete
                </button>
            </div>
            <div className="taskdetail">
                <div className="">
                    <h4>{taskdetail.task_title}</h4>
                </div>
                <div className="">
                    <h6>{taskdetail.task_description}</h6>
                </div>
                <div className="">
                    {taskdetail.priority}
                </div>
                <div className="">
                    {taskdetail.deadline}
                </div>
                <div className="">
                    {taskdetail.status}
                </div>
            </div>

            <div className="comment row">
                <h3>Comments :</h3>
               {comments.map((comment)=>{
                   return (
                    <div className="cmt-desc"> Description :{comment.comment_description}
                        <button className="btn btn-sm btn-danger btn-project-details float-end" >
                            Delete
                        </button>
                    </div>      
                   )
               })}
               <div>
               <div className="form">
                    <div className="mb-3">
                    <label className="form-label">Contents</label>
                    <textarea
                        className="form-control"
                        rows="3"></textarea>
                    </div>
                    <div className="mb-3">
                    <button className="btn btn-success">
                        Add Comment
                    </button>
                    <button className="btn btn-danger float-end">
                        Cancel
                    </button>
                    </div>
                </div>
               </div>
            </div>
        </div>
    )
}

export default TaskDetailScreen