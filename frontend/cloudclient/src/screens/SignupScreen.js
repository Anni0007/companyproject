import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'
import { Link } from 'react-router-dom'
import { signUpUser } from '../api'
import Header from '../components/Header'

const SignupScreen = (props) => {
	let history = useHistory()
	const [Name, setName] = useState('')
	const [Email, setEmail] = useState('')
	const [Password, setPassword] = useState('')
	const signUp =async ()=> {
		const payload = {
			name: Name,
			email:Email,
			password:Password
		}
		console.log(payload)
		const data = await signUpUser(payload)
		if (data.status === 'success'){
			history.push('/signin')
		}
	}
			return (
		<div>
			<Header title="Signup" />
			<div className="form">
				<div className="mb-3">
					<label className="form-label">Name</label>
					<input
						className="form-control" onChange={(e)=>setName(e.target.value)}></input>
				</div>
				<div className="mb-3">
					<label className="form-label">Email</label>
					<input
						type="email"
						className="form-control"
						placeholder="test@test.com"
						onChange={(e)=>setEmail(e.target.value)}
					/>
				</div>
				<div className="mb-3">
					<label className="form-label">Password</label>
					<input
						type="password"
						className="form-control"
						placeholder="*****"
						onChange={(e)=>setPassword(e.target.value)}></input>
				</div>
				<div className="mb-3">
					<button className="btn btn-success" onClick={signUp}>
						Singup
					</button>
					<div className="float-end">
						Existing user? <Link to="/signin">Signin here</Link>
					</div>
				</div>
			</div>
		</div> 
		)
}
        
export default SignupScreen